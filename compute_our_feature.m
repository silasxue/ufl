
function [ im_codes , im_pooling ] = compute_our_feature( im )

        dic_first.patchsize = 11;  % (67 23 3 )   (7 1 1 )
        dic_first.stepnum = 11; % in a big patch
        dic_first.stepsize = 1;  % in a big patch
        dic_first.dicsize = 100;
       
        pooling_size = 7; 
        whitening = 1;
        dic_file = 'demo_dic_kmeans_11x11_sn11_dic100_rgb.mat';
        load(dic_file);
   %     printf('start to encode  ... ...>>>>>>>>>>>>>>>>>>>>>>>>>');
        dic_first.dic = dic;    
%        dic_first.iteration = 100; 
%        dic_first.lambda = 0.15;  

        if size(im,3) == 1
          im = color(im);
        end
        im = im2double(im);

        im_codes = coding_kmeans(im, dic_first, M,P,whitening);
        disp('finish coding ..............');
        im_pooling = pooling_kmeans(im_codes, pooling_size);
        disp('finish pooling  ............');
   
end




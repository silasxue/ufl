// matlab include
#include "mex.h"
#include "matrix.h"

// c++ include
#include "DT.h"
#include "SPNodeMatch.h"

#include <ctime>
#include <iostream>
#include <cmath>

#include "omp.h"
#include "openmpflag.h"

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], 
				 int nrhs, const mxArray *prhs[])
// rhs (input): 
// rhs[0]: feat1 (im1_height by im1_width by feat_dim, single type)
// rhs[1]: feat2 (im2_height by im2_width by feat_dim, single type)
// rhs[2]: params
// lhs (output):
// lhs[0]: node_cost (n_disp by n_node )
// lhs[1]: node disparity (n_disp by 1)
{
	if ( nrhs != 3 ) { 
		mexPrintf("the number of input arg must be 3\n");
		return;
	}
	if ( ! mxIsSingle(prhs[0]) ||  ! mxIsSingle(prhs[1]) ) {
		mexPrintf("wrong descriptor format: must be single\n");
		return;
	}
	
	mwSize n_dim1 = mxGetNumberOfDimensions(prhs[0]);
	if ( n_dim1 != 3) {
		mexPrintf("wrong feature format -- it must be heightxwidthxfeat_dim\n");
        return;
	}
    size_t feat_dim1, height1, width1;
    const mwSize* dims1 = mxGetDimensions(prhs[0]);
    height1 = dims1[0];
    width1 = dims1[1];
    feat_dim1 = dims1[2];// sift1 dimension
    mexPrintf("sift1: height1 = %d, width1 = %d, feat_dim1 = %d\n", height1, width1, feat_dim1);
	mwSize n_dim2 = mxGetNumberOfDimensions(prhs[1]);
	if ( n_dim2 != 3) {
		mexPrintf("wrong feature format -- it must be heightxwidthxfeat_dim\n");
        return;
	}
	size_t feat_dim2, height2, width2;
    const mwSize* dims2 = mxGetDimensions(prhs[1]);
    height2 = dims2[0];
    width2 = dims2[1];
    feat_dim2 = dims2[2]; // sift2 dimension
    mexPrintf("sift2: height2 = %d, width2 = %d, feat_dim2 = %d\n", height2, width2, feat_dim2);

	if ( feat_dim1 != feat_dim2 ){
		mexPrintf("different feature dimension\n");
		return;
	}
	size_t feat_dim = feat_dim1;
	
	// get descriptors
	float* feat1 = (float*)mxGetData(prhs[0]);//sift1
	float* feat2 = (float*)mxGetData(prhs[1]);//sift2
	
	// get parameter values
	mxArray *tmp;
	tmp = mxGetField(prhs[2], 0, "n_tree_level");
	int n_tree_level = (int)mxGetScalar(tmp);   // 3

	tmp = mxGetField(prhs[2], 0, "search_radius");
	double* search_radius = mxGetPr(tmp);
	int search_radius_x = (int)search_radius[0];
	int search_radius_y = (int)search_radius[1];
	
	tmp = mxGetField(prhs[2], 0, "grid_size");
	int grid_size = (int)mxGetScalar(tmp);
	
	tmp = mxGetField(prhs[2], 0, "truncate_const");
	double truncate_const = mxGetScalar(tmp);

    mexPrintf("n_tree_level = %d, search_radius_x = %d, search_radius_y = %d \n", n_tree_level, search_radius_x, search_radius_y);
    mexPrintf("grid_size = %d, truncate_const = %f \n", grid_size, truncate_const);

	
	// set parameters
	MatchParams params;
	params.sample_grid_size = grid_size;
	params.n_tree_level = n_tree_level;
	params.search_size_x = search_radius_x;
	params.search_size_y = search_radius_y;
	params.truncate_const = truncate_const;

	SPNodeMatch sp_match;
	int n_node = (int)pow(4.0,n_tree_level)/3;
    mexPrintf("n_node = %d \n", n_node);
	
	ImSize im_size1, im_size2;
	im_size1.width = width1;
	im_size1.height = height1;
	im_size2.width = width2;
	im_size2.height = height2;

	int n_dist_mat_xy_elem = (im_size1.height/grid_size)*(im_size1.width/grid_size);
	int n_dist_mat_dx = (2*params.search_size_x)/grid_size + 1;
	int n_dist_mat_dy = (2*params.search_size_y)/grid_size + 1;
	int n_disp = n_dist_mat_dx * n_dist_mat_dy;
    mexPrintf("n_dist_mat_xy_elem = %d \n", n_dist_mat_xy_elem);
    mexPrintf("n_dist_mat_dx = %d \n", n_dist_mat_dx);
    mexPrintf("n_dist_mat_dy = %d \n", n_dist_mat_dy);
    mexPrintf("n_disp = %d \n", n_disp);

	// output1 -- node's match cost
	mwSize m = n_disp;
	mwSize n = n_node;
	plhs[0] = mxCreateNumericMatrix(m, n, mxSINGLE_CLASS, mxREAL);
	float* node_cost = (float*)mxGetData(plhs[0]);
	
	// output2 -- node's disparity
	m = 2;
	n = n_disp;
	plhs[1] = mxCreateNumericMatrix(m, n, mxINT32_CLASS, mxREAL);
	int* node_disparity = (int*)mxGetData(plhs[1]);

	// output3 -- auxilary info
	m = 1;
	n = 2;
	plhs[2] = mxCreateNumericMatrix(m, n, mxINT32_CLASS, mxREAL);
	int *aux_info = (int*)mxGetData(plhs[2]);
	aux_info[0] = n_dist_mat_dx;
	aux_info[1] = n_dist_mat_dy;


	// run match

	Descriptor descriptor1;
	descriptor1.desc = feat1;
	descriptor1.width = width1;
	descriptor1.height = height1;
	descriptor1.desc_dim = feat_dim;

	Descriptor descriptor2;
	descriptor2.desc = feat2;
	descriptor2.width = width2;
	descriptor2.height = height2;
	descriptor2.desc_dim = feat_dim;

	//sp_match.NodeMatch(descriptor1, descriptor2, params, node_cost, node_disparity);
	sp_match.NodeMatchWithMaxVector(descriptor1, descriptor2, params, node_cost, node_disparity);
	
}


function []=demo_one(im1_file,im2file)
close all
im1=imread(im1_file);
im2=imread(im2file);

im1=im2double(im1);
im2=im2double(im2);
%figure;imshow(im1);figure;imshow(im2);

[ im_codes1 , im_pooling1 ] = compute_our_feature(  (im1) );
[ im_codes2 , im_pooling2 ] = compute_our_feature(  (im2) );

[ vx , vy,time_count,match_param] = dense_matching(im1,im2,im_codes1 , im_pooling1 ,im_codes2 , im_pooling2 );
my_matching.vx = vx;
my_matching.vy = vy;

warpI2=warpImage(im2,vx,vy);
figure;imshow(im1);figure;imshow(im2);figure;imshow(warpI2);

% display flow
% clear flow;
% flow(:,:,1)=vx;
% flow(:,:,2)=vy;
% figure;imshow(flowToColor(flow));

end


